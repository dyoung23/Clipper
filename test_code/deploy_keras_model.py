from __future__ import absolute_import, print_function
import os
import sys
cur_dir = os.path.dirname(os.path.abspath(__file__))

from test_utils import (create_docker_connection, BenchmarkException, headers,
                        log_clipper_state)
cur_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.abspath("%s/../clipper_admin" % cur_dir))
from clipper_admin.deployers.keras import deploy_keras_model, create_endpoint

from clipper_admin import ClipperConnection,DockerContainerManager

clipper_conn = ClipperConnection(DockerContainerManager())
try:
	clipper_conn.start_clipper()
except Exception, e:
	print(e)
	clipper_conn.stop_all()

clipper_conn.register_application(name="hello-keras", input_type="bytes", default_output="this is default output", slo_micros=100000)

#clipper_conn.get_all_apps()

def predict(model,inputs):
    import base64
    import io
    import tempfile
    import numpy as np
    from keras.preprocessing.image import load_img, img_to_array

    num_imgs = len(inputs)
    preds = []
    for i in range(num_imgs):
        # Create a temp file to write to
        tmp = tempfile.NamedTemporaryFile('wb', delete=False, suffix='.png')
        tmp.write(io.BytesIO(inputs[i]).getvalue())
        tmp.close()
  
	#read image and convert image
        image = load_img(tmp.name,grayscale=True,target_size=(28,28))
	image = img_to_array(image)
	image = np.expand_dims(image, axis=0)
	image = image.astype('float32')
	image /= 255
  
        #predict
	pred = model.predict(image)      
        preds.append(pred)
    return [str(p) for p in preds]

deploy_keras_model(clipper_conn, name="keras-model", version=1, input_type="bytes", func=predict,keras_model_path='/home/dyc/clipper/models/',base_image='docker.io/clipper/keras-container:develop',registry="docker.io/clipper",pkgs_to_install=['pillow'])

clipper_conn.link_model_to_app(app_name="hello-keras", model_name="keras-model")
